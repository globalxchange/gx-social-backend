import { ApolloServer, PubSub } from "apollo-server-express";
import express from "express";
import http from "http";

import Query from "./resolvers/Query";
import Mutation from "./resolvers/Mutation";

import typeDefs from "./schema.graphql";
import Post from "./resolvers/Post";
import Trend from "./resolvers/Trend";
import User from "./resolvers/User";
import Comment from "./resolvers/Comment";

// import db from "./db";
const db = require("./connections/mongodb.js");

db.connect(function(err) {
  if (err) {
    console.log("Database Connection Failed");
    process.exit(1);
  } else {
    // import User from "./resolvers/User";
    // import Comment from "./resolvers/Comment";

    // import Subscription from "./resolvers/Subscription";

    const app = express();

    // var myLogger = function(req, res, next) {
    //   console.log("LOGGED");
    //   next();
    //   //   res.send("Error");
    // };

    // app.use(myLogger);

    app.get("/", (req, res, next) => {
      res.send("Yayyy Server woeking");
    });

    // const pubsub = new PubSub();

    const server = new ApolloServer({
      typeDefs,
      resolvers: {
        Query,
        Mutation,
        Post,
        Trend,
        User,
        Comment
      },
      context: async ({ req }) => {
        // throw new Error("un authorised user");
        return {
          db: db.get(),
          req: req
        };
      },
      playground: true
    });

    server.applyMiddleware({ app });

    const httpServer = http.createServer(app);
    server.installSubscriptionHandlers(httpServer);

    httpServer.listen({ port: 3997 }, () => {
      console.log(
        `🚀 Server ready at http://localhost:3997${server.graphqlPath}`
      );
      console.log(
        `🚀 Subscriptions ready at ws://localhost:3997${server.subscriptionsPath}`
      );
    });
  }
});

// app.disable("x-powered-by");
