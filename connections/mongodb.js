const mongodb = require("mongodb");
let ObjectID = mongodb.ObjectID;
const config = require("../config.js");

var state = {
  db: null
};

module.exports.connect = function(done) {
  if (state.db) return done();

  mongodb.MongoClient.connect(
    config.mongo_url,
    { useNewUrlParser: true, useUnifiedTopology: true },
    function(err, database) {
      if (err) {
        console.log(err);
        process.exit(1);
      }
      // Save database object from the callback for reuse.
      let client = database.db(config.database_name);
      state.db = client;
      done();
      //  /console.log(db)
      console.log("Database connection ready");
    }
  );
};

module.exports.get = function() {
  return state.db;
};
