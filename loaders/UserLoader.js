import DataLoader from "dataloader";
import db_obj from "../connections/mongodb";

const fetch_user = async id => {
  const db = db_obj.get();
  console.log("llllllllllllllllllll", id);

  let temp = await db
    .collection("user_profile")
    .aggregate([
      {
        $match: {
          id: id
        }
      },
      { $unwind: "$following_users" },
      { $project: { _id: 1, following_users: 1 } },
      {
        $group: {
          _id: "$_id",
          following_users: {
            $push: {
              id: "$following_users.id",
              tier_id: "$following_users.tier_id",
              timestamp: "$following_users.timestamp"
            }
          }
        }
      }
    ])
    .toArray();
  // console.log(temp[0].following_users);
  return temp[0].following_users;
};

const user_loader = new DataLoader(async keys => {
  console.log(keys);
  return Promise.all(keys.map(fetch_user));
});

export { user_loader as default };
