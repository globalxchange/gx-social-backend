const Comment = {
  author: async (parent, args, { db }, info) => {
    try {
      console.log("444444444444444444444444444444", parent.user_id);
      let result = await db
        .collection("user_profile")
        .find({ id: parent.user_id })
        .project({
          id: 1,
          name: 1,
          username: 1,
          bio: 1,
          timestamp: 1,
          date_of_birth: 1,
          profile_image: 1,
          email: 1
        })
        .toArray();

      return result[0];
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the author of the comment");
    }
  },
  post: async (parent, args, { db }, info) => {
    try {
      return await db.collection("posts").findOne({ id: parent.post_id });
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching post of the given comment");
    }
  },
  liked_users: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("user_profile")
        .find({ id: { $in: [...parent.liked_users] } })
        .project({
          id: 1,
          name: 1,
          username: 1,
          bio: 1,
          timestamp: 1,
          date_of_birth: 1,
          profile_image: 1,
          email: 1
        })
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching liked users");
    }
  }
};

export { Comment as default };
