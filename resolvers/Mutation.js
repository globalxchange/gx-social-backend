import uuidv4 from "uuid/v4";
import { simple_hash } from "../utils/hash_generator";

const Mutation = {
  register_user: async (parent, args, { db }, info) => {
    const {
      name,
      username,
      bio,
      date_of_birth,
      profile_image,
      cover_image,
      email
    } = args.data;

    let new_user = {
      id: simple_hash(email),
      name,
      username,
      email: email,
      bio,
      date_of_birth,
      profile_image,
      timestamp: Date.now(),
      cover_image,

      following_count: 0,
      my_follower_count: 0,

      following_users: [],
      my_followers: []
    };

    try {
      await db.collection("user_profile").insertOne({ ...new_user });
      return new_user;
    } catch (e) {
      console.log(e);
      throw new Error("Error in saving user");
    }
  },

  update_user: async (parent, args, { db }, info) => {
    let { email } = args.data;
    let id = simple_hash(email);
    let updated_user_data = { ...args.data };
    delete updated_user_data.email;

    try {
      let updated_document = await db
        .collection("user_profile")
        .findOneAndUpdate(
          { id },
          { $set: { ...updated_user_data } },
          { returnOriginal: false }
        );
      updated_document = updated_document.value;
      delete updated_document._id;
      console.log(updated_document);
      return updated_document;
    } catch (e) {
      console.log(e);
      throw new Error("Profile updation failed");
    }
  },

  follow_user: async (parent, args, { db }, info) => {
    try {
      let my_id = simple_hash(args.data.email);
      let other_user_id = simple_hash(args.data.to_be_followed_user_email);

      let updated_user_doc = await db
        .collection("user_profile")
        .findOneAndUpdate(
          { id: my_id },
          {
            $push: {
              following_users: {
                id: other_user_id,
                tier_id: args.data.tier_id,
                timestamp: Date.now() //timestamp to identify from when does this user started following
              }
            },
            $inc: { following_count: 1 }
          }
        );

      let another_updated_user_doc = await db
        .collection("user_profile")
        .findOneAndUpdate(
          { id: other_user_id },
          {
            $push: {
              my_followers: {
                id: my_id,
                tier_id: args.data.tier_id,
                timestamp: Date.now() //timestamp to identify from when does this user started following
              }
            },
            $inc: { my_follower_count: 1 }
          }
        );

      console.log(updated_user_doc);
      if (
        updated_user_doc.lastErrorObject.n === 1 &&
        updated_user_doc.ok === 1 &&
        another_updated_user_doc.lastErrorObject.n === 1 &&
        another_updated_user_doc.ok === 1
      ) {
        return true;
      } else {
        return new Error("Unable to follow the requested user");
      }
    } catch (e) {
      console.log(e);
      throw new Error("Unable to follow the requested user");
    }
  },

  new_trend: async (parent, args, { db }, info) => {
    let new_trend = {
      id: uuidv4(),
      name: args.data.name,
      post_count: 0
    };
    try {
      await db.collection("trends").insertOne({ ...new_trend });
      return new_trend;
    } catch (e) {
      console.log(e);
      throw new Error("Error in saving a new trend");
    }
  },

  create_post: async (parent, args, { db }, info) => {
    // console.log(args.new_trend.name);
    try {
      let new_trend_id = uuidv4();
      let author_id = simple_hash(args.data.email);

      const new_post = {
        id: uuidv4(),
        timestamp: Date.now(),
        like_count: 0,
        title: args.data.title,
        body: args.data.body,
        post_tier_id: args.data.post_tier_id,
        author_id,
        liked_users: [],
        comment_count: 0
      };
      //if there exists a new trend in the post

      if (args.new_trend) {
        console.log(args.new_trend.name);
        await db.collection("trends").insertOne({
          id: new_trend_id,
          name: args.new_trend.name,
          post_count: 1
        });
        new_post.trends = [...args.data.trends, new_trend_id];
      } else {
        new_post.trends = [...args.data.trends];
      }

      await db
        .collection("trends")
        .updateMany(
          { id: { $in: [...args.data.trends] } },
          { $inc: { post_count: 1 } }
        );

      await db.collection("posts").insertOne({ ...new_post });

      return new_post;
    } catch (e) {
      console.log(e);
      throw new Error("Error in creating a new post");
    }
  },

  delete_post: async (parent, args, { db }, info) => {
    try {
      // let post_to_be_deleted = await db
      //   .collection("posts")
      //   .findOne({ id: args.data.post_id });
      let author_id = simple_hash(args.data.email);

      let delete_post_statistics = await db
        .collection("posts")
        .findOneAndDelete({ id: args.data.post_id, author_id });
      console.log(delete_post_statistics);
      // return true;

      await db
        .collection("trends")
        .findOneAndUpdate(
          { id: { $in: delete_post_statistics.value.trends } },
          { $inc: { post_count: -1 } }
        );

      if (
        delete_post_statistics.ok === 1 &&
        delete_post_statistics.lastErrorObject.n === 1
      ) {
        if (delete_post_statistics.value.comment_count > 0) {
          let delete_comments_statistics = await db
            .collection("comments")
            .deleteMany({ post_id: args.data.post_id });

          if (delete_comments_statistics.deletedCount > 0) {
            return true;
          } else {
            return new Error("Cannot delete ");
          }
        } else {
          return true;
        }
      } else {
        return new Error("post_id or user_id is invalid");
      }
    } catch (e) {
      console.log(e);
      throw new Error("Error in deleting post");
    }
  },

  create_comment: async (parent, args, { db }, info) => {
    let user_id = simple_hash(args.data.email);
    let new_comment = {
      id: uuidv4(),
      ...args.data,
      timestamp: Date.now(),
      reported: false,
      user_id,
      like_count: 0,
      liked_users: []
    };
    delete new_comment.email;

    try {
      await db.collection("comments").insertOne({ ...new_comment });
      await db
        .collection("posts")
        .updateOne({ id: args.data.post_id }, { $inc: { comment_count: 1 } });
      return new_comment;
    } catch (e) {
      console.log(e);
      throw new Error("Error in adding comment");
    }
  },

  delete_comment: async (parent, args, { db }, info) => {
    try {
      // decrementing comment count to which this comment belong to.

      //deleting the comment from comments collection.
      let user_id = simple_hash(args.data.email);
      let deleted_doc_statistics = await db
        .collection("comments")
        .deleteOne({ id: args.data.comment_id, user_id });
      if (deleted_doc_statistics.deletedCount > 0) {
        await db
          .collection("posts")
          .updateOne(
            { id: args.data.post_id },
            { $inc: { comment_count: -1 } }
          );
        return true;
      } else return new Error("comment_id or user_id invalid");
    } catch (e) {
      console.log(e);
      throw new Error();
    }
  },

  like_dislike_for_comment: async (parent, args, { db }, info) => {
    try {
      let id = simple_hash(args.data.email);
      if (!args.dislike) {
        //i.e if its like operation
        await db.collection("comments").updateOne(
          { id: args.data.comment_id },
          {
            $inc: { like_count: 1 },
            $addToSet: { liked_users: id }
          }
        );
        return true;
      } else {
        //i.e if its a dislike operation
        await db.collection("comments").updateOne(
          { id: args.data.comment_id },
          {
            $inc: { like_count: -1 },
            $pull: { liked_users: id }
          }
        );
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error("Error in like/dislike operation of comment ");
    }
  },

  like_dislike_for_post: async (parent, args, { db }, info) => {
    try {
      let id = simple_hash(args.data.email);
      if (!args.dislike) {
        //i.e if its a like operation

        await db.collection("posts").updateOne(
          { id: args.data.post_id },
          {
            $inc: { like_count: 1 },
            $addToSet: { liked_users: id }
          }
        );
        return true;
      } else {
        //i.e if its a dislike operation
        await db.collection("posts").updateOne(
          { id: args.data.post_id },
          {
            $inc: { like_count: -1 },
            $pull: {
              liked_users: id
            }
          }
        );
        return true;
      }
    } catch (e) {
      console.log(e);
      throw new Error("Error in like/dislike operation of post");
    }
  }
};

export { Mutation as default };
