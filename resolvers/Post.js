const Post = {
  trends: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("trends")
        .find({ id: { $in: [...parent.trends] } })
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching trends");
    }
  },
  author: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("user_profile")
        .findOne({ id: parent.author_id });
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the author of the post");
    }
  },
  comments: async (parent, args, { db }, info) => {
    try {
      console.log(parent.comments);
      if (parent.comments !== null && parent.comments.length > 0)
        return await db
          .collection("comments")
          .find({ post_id: parent.id })
          .toArray();
      else return [];
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the post comments");
    }
  },
  liked_users: async (parent, args, { db }, info) => {
    console.log("here", parent.liked_users);
    try {
      if (parent.liked_users !== null && parent.liked_users.length > 0)
        return await db
          .collection("user_profile")
          .find({ id: { $in: [...parent.liked_users] } })
          .project({
            id: 1,
            name: 1,
            username: 1,
            bio: 1,
            timestamp: 1,
            date_of_birth: 1,
            profile_image: 1,
            email: 1
          })
          .toArray();
      else return [];
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching liked users");
    }
  }
};

export { Post as default };
