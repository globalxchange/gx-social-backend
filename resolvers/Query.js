import tier_resolver from "../utils/tier_resolver";
import { simple_hash } from "../utils/hash_generator";

const Query = {
  get_user: async (parent, args, { db }, info) => {
    try {
      let user = await db
        .collection("user_profile")
        .findOne({ id: args.data.id });
      if (user !== null) {
        return user;
      } else {
        return new Error("user_id is invalid or unable to fetch user");
      }
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching user");
    }
  },
  follow_suggestions: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("user_profile")
        .find({})
        .sort({ timestamp: -1 })
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the follow_suggestions");
    }
  },
  trends: async (parent, args, { db, data_store }, info) => {
    try {
      console.log("999999999999999999");
      data_store = { name: "kkkkkk" };
      // console.log(args.data);
      return await db
        .collection("trends")
        .find({})
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching trends");
    }
  },

  posts: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("posts")
        .find({})
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching posts");
    }
  },

  get_user_posts: async (parent, args, { db }, info) => {
    try {
      let current_user_id = simple_hash(args.data.current_user_email);
      let requested_user_id = simple_hash(args.data.requested_user_email);

      let requested_user = await db
        .collection("user_profile")
        .aggregate([
          {
            $match: {
              id: requested_user_id,
              "my_followers.id": current_user_id
            }
          },
          { $unwind: "$my_followers" },
          {
            $match: {
              "my_followers.id": current_user_id
            }
          },
          { $project: { my_followers: 1, _id: 0 } }
        ])
        .toArray();

      console.log(requested_user[0].my_followers.tier_id);

      let pipeline = await db
        .collection("user_profile")
        .aggregate([
          {
            $match: {
              id: requested_user_id,
              "my_followers.id": current_user_id
            }
          },
          {
            $project: { id: 1, my_followers: 1 }
          },
          {
            $graphLookup: {
              from: "posts",
              startWith: "$id",
              connectFromField: "id",
              connectToField: "author_id",
              as: "alias"
            }
          },
          {
            $project: {
              alias: 1,
              id: 1,
              my_followers: 1
            }
          },
          { $unwind: "$alias" },
          { $unwind: "$my_followers" },
          {
            $match: {
              "my_followers.id": current_user_id
            }
          },
          {
            $group: {
              _id: "$_id",
              posts: {
                $push: {
                  $cond: {
                    if: {
                      // $eq: ["$my_followers.tier_id", "$alias.post_tier_id"]
                      $in: [
                        "$alias.post_tier_id",
                        tier_resolver([requested_user[0].my_followers.tier_id])
                      ]
                    },
                    then: {
                      id: "$alias.id",
                      author_id: "$alias.author_id",
                      title: "$alias.title",
                      timestamp: "$alias.timestamp",
                      body: "$alias.body",
                      trends: "$alias.trends",
                      comment_count: "$alias.comment_count",
                      like_count: "$alias.like_count",
                      liked_users: "$alias.liked_users",
                      post_tier_id: "$alias.post_tier_id",
                      locked: false
                    },
                    else: {
                      id: "$alias.id",
                      author_id: "$alias.author_id",
                      title: "$alias.title",
                      timestamp: "$alias.timestamp",
                      body: null,
                      trends: "$alias.trends",
                      comment_count: "$alias.comment_count",
                      like_count: "$alias.like_count",
                      liked_users: null,
                      post_tier_id: "$alias.post_tier_id",
                      locked: true
                    }
                  }
                }
              }
            }
          }
          // {
          //   $project: {
          //     posts: {
          //       $filter: {
          //         input: "$posts",
          //         as: "post",
          //         cond: { $ne: ["$$post", "not required"] }
          //       }
          //     }
          //   }
          // }
        ])
        .toArray();

      console.log(pipeline);

      return pipeline[0].posts;

      // let doc = await db.collection("user_profile").findOne({
      //   id: args.data.requested_user_id,
      //   "my_followers.id": args.data.current_user_id
      // });
      // console.log(doc);

      // if (doc !== null || doc !== undefined) {
      //   let s = await db
      //     .collection("posts")
      //     .find({
      //       post_tier_id: doc.my_followers[0].tier_id,
      //       author_id: doc.id
      //     })
      //     .toArray();
      //   console.log(s);
      //   return s;
      // }
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching posts");
    }
  },
  user_choosen_trend: async (parent, args, { db, req }, info) => {
    try {
      let user_id = simple_hash(args.data.email);
      console.log(user_id);
      let result = await db
        .collection("user_profile")
        .aggregate([
          {
            $match: {
              id: user_id
            }
          },
          { $unwind: "$following_users" },
          { $project: { _id: 1, following_users: 1 } },
          {
            $group: {
              _id: "$_id",
              following_users: {
                $push: {
                  id: "$following_users.id",
                  tier_id: "$following_users.tier_id",
                  timestamp: "$following_users.timestamp"
                }
              }
            }
          }
        ])
        .toArray();

      let following_users = result[0].following_users;

      console.log(following_users);

      following_users.map(item => {
        console.log([item.tier_id]);
        item.tier_id = tier_resolver([item.tier_id]); //setting sub tier ids to the root tier_id as array
      });

      console.log(following_users);

      // console.log(following_users);
      // let tiers = tier_resolver(tier_ids);
      // console.log(tiers);

      let posts_of_given_trend = await db
        .collection("posts")
        .find({
          trends: args.data.trend_id,
          author_id: { $ne: user_id }
        })

        .toArray();

      await Promise.all(
        posts_of_given_trend.map(item => {
          let following_user = following_users.find(
            i => i.id === item.author_id
          );

          if (following_user !== null && following_user !== undefined) {
            let tier_ids_set = new Set(following_user.tier_id);
            if (tier_ids_set.has(item.post_tier_id)) {
              item["locked"] = false;
              item["comments"] = "allow";
            } else {
              item["locked"] = true;
              item["body"] = null;
              item["liked_users"] = null;
              item["comments"] = null;
            }
          } else {
            item["locked"] = true;
            item["body"] = null;
            item["liked_users"] = null;
            item["comments"] = null;
          }
        })
      );

      console.log("posts", posts_of_given_trend);

      return posts_of_given_trend;
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching posts");
    }
  },
  get_my_home_posts: async (parent, args, { db, req }, info) => {
    let user_id = simple_hash(args.data.email);
    let following_user_ids = [];

    let result = await db
      .collection("user_profile")
      .aggregate([
        {
          $match: {
            id: user_id
          }
        },
        { $unwind: "$following_users" },
        { $project: { _id: 1, following_users: 1 } },
        {
          $group: {
            _id: "$_id",
            following_users: {
              $push: {
                id: "$following_users.id",
                tier_id: "$following_users.tier_id",
                timestamp: "$following_users.timestamp"
              }
            }
          }
        }
      ])
      .toArray();

    let following_users = result[0].following_users;

    // console.log(following_users);

    following_users.map(item => {
      // console.log([item.tier_id]);
      item.tier_id = tier_resolver([item.tier_id]); //setting sub tier ids to the root tier_id as array
      following_user_ids.push(item.id);
    });

    console.log(following_users);
    console.log(following_user_ids);

    let posts_of_following_users = await db
      .collection("posts")
      .find({ author_id: { $in: following_user_ids } })
      .toArray();
    console.log(posts_of_following_users);

    await Promise.all(
      posts_of_following_users.map(item => {
        let following_user = following_users.find(i => i.id === item.author_id);

        if (following_user !== null && following_user !== undefined) {
          let tier_ids_set = new Set(following_user.tier_id);
          if (tier_ids_set.has(item.post_tier_id)) {
            item["locked"] = false;
            item["comments"] = "allow"; //will be used in child resolver to take a decision of fetch or not to fetch
          } else {
            item["locked"] = true;
            item["body"] = null;
            item["liked_users"] = null;
            item["comments"] = null;
          }
        } else {
          item["locked"] = true;
          item["body"] = null;
          item["liked_users"] = null;
          item["comments"] = null;
        }
      })
    );

    return posts_of_following_users;
  },
  get_my_posts: async (parent, args, { db, req }, info) => {
    try {
      let user_id = simple_hash(args.data.email);
      let my_posts = await db
        .collection("posts")
        .find({ author_id: user_id })
        .toArray();
      console.log(my_posts);

      await Promise.all(
        my_posts.map(item => {
          item["comments"] = "allow"; //will be used in child resolver to take a decision of fetch or not to fetch
        })
      );
      return my_posts;
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching user posts");
    }
  }
};

export { Query as default };
