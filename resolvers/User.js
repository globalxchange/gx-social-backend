const User = {
  authored_posts: async (parent, args, { db }, info) => {
    console.log(parent.id);
    try {
      return await db
        .collection("posts")
        .find({ author_id: parent.id })
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching User posts");
    }
  },
  comments: async (parent, args, { db }, info) => {
    try {
      return await db
        .collection("comments")
        .find({ user_id: parent.id })
        .toArray();
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching User posts");
    }
  },
  following_users: async (parent, args, { db }, info) => {
    try {
      let doc = await db.collection("user_profile").findOne({ id: parent.id });
      console.log(doc.following_users);
      return doc.following_users;
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the following_users");
    }
  },
  my_followers: async (parent, args, { db }, info) => {
    try {
      let doc = await db.collection("user_profile").findOne({ id: parent.id });
      console.log(doc.my_followers);
      return doc.my_followers;
    } catch (e) {
      console.log(e);
      throw new Error("Error in fetching the my_follow_users");
    }
  }
};
export { User as default };
