const crypto = require("crypto");
import { hash_secret } from "../config";

const simple_hash = data => {
  return crypto
    .createHash("md5")
    .update(data + hash_secret)
    .digest("hex");
};

// const unique_hash_timestamp = data => {
//   return crypto
//     .createHash("sha256")
//     .update(data + Date.now().toString())
//     .digest("hex");
// };

module.exports = { simple_hash };
