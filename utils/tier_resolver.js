/**
 * @author: techkrtk
 * @description: this resolves the tiers, ie for example if the input tier is tier1 then it sends back all the tiers belowe tier1 including tier1
 */

const tier_resolver = ids => {
  const tier_0 = "e4a9510f-208f-4149-b7c5-5cb846b469e7";
  const tier_1 = "00b94738-cac8-4e7c-9a9b-7afd221dd2ed";
  const tier_2 = "7446b3c1-89b2-466f-8e4d-bcdc89d3f29b";
  const tier_3 = "35812723-07dc-48b8-ab02-cba074680ae2";

  const get_sub_tiers = id => {
    if (id === tier_0) return [tier_0];
    else if (id === tier_1) return [tier_0, tier_1];
    else if (id === tier_2) return [tier_0, tier_1, tier_2];
    else if (id === tier_3) return [tier_0, tier_1, tier_2, tier_3];
    else return new Error("Invalid tier");
  };

  // console.log("input", ids);

  let tier_set = new Set([]);

  let tier_ids = [...ids];
  tier_ids.map(id => {
    let result = get_sub_tiers(id);
    tier_set = new Set([...tier_set, ...result]); //unioining the existing set with new result
  });

  // console.log([...tier_set]);
  return [...tier_set];
};

export { tier_resolver as default };
